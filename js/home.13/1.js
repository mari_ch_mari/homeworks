const images = document.querySelectorAll(".image-to-show");
const stopButton = document.querySelector("#stop-bth");
const resumeButton = document.querySelector("#resume-btn");

let currentIndex = 0;
let intervalId = null; 
let stopped = false; 

function showNextImage() {
  for (let i = 0; i < images.length; i++) {
    images[i].style.display = "none";
  }

  currentIndex++;
  if (currentIndex >= images.length) {
    currentIndex = 0;
  }

  images[currentIndex].style.display = "block";
}

function startImageRotation() {
  intervalId = setInterval(() => {
    showNextImage();
  }, 3000);
}

stopButton.addEventListener("click", () => {
  clearInterval(intervalId); 
  stopped = true; 
});

resumeButton.addEventListener("click", () => {
  if (!stopped) return; 
  startImageRotation(); 
  stopped = false; 
});

startImageRotation();
