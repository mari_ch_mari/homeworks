const theme = window.localStorage.getItem("theme")
if(theme === "dark"){
    document.body.classList.add(theme)
}
const button = document.getElementById("button")
button.addEventListener("click", (event)=>{
    event.preventDefault();
    document.body.classList.toggle("dark")
    if(theme === "dark"){
        window.localStorage.setItem("theme","light")
    }else
    
        window.localStorage.setItem("theme","dark")
});
localStorage.setItem("theme", "dark")
localStorage.getItem("theme")