const paragraphs = document.querySelectorAll('p');
for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = '#ff0000';
}

const optionsList = document.getElementById('optionsList');
console.log(optionsList);

const parent = optionsList.parentNode;
console.log(parent);

const children = optionsList.querySelectorAll('*');
children.forEach((child) => {
    console.log(child.nodeName, child.nodeType);
});

const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = 'This is a paragraph';

const mainHeader = document.querySelector('.main-header');
const childNodes = mainHeader.childNodes;

childNodes.forEach(node => {
    if (node.nodeType === Node.ELEMENT_NODE) {
        node.classList.add('nav-item');
    }
});
console.log(childNodes);

const header = document.querySelector('.main-header');

header.querySelectorAll('*').forEach((element) => {
    element.classList.add('nav-item');
    console.log(element);
});

const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach((element) => {
    element.classList.remove('selction-title')
});