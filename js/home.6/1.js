function createNewUser() {
    const user = {};
    user.firstName = prompt("Введіть своє ім'я:");
    user.lastName = prompt("Введіть своє прізвище:");
    user.birthday = prompt("Введіть дату свого народження у форматі dd.mm.yyyy:");
    
    user.getAge = function() {
      const today = new Date();
      const birthdayDay = +this.birthday.substring(0, 2)
      const birthdayMonth = +this.birthday.substring(3, 5)
      const birthdayYear = +this.birthday.substring(6, 10)
      const birthdate = new Date(birthdayYear, birthdayMonth, birthdayDay);
      let age = today.getFullYear() - birthdate.getFullYear();
     
      const monthDiff = today.getMonth() - birthdate.getMonth() + 1;
    
      if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthdate.getDate())) {
        age--;
      }
      
    return age
    };
    
    user.getPassword = function() {
      const firstLetter = this.firstName.charAt(0).toUpperCase();
      const lastNameLower = this.lastName.toLowerCase();
      const year = new Date(this.birthday).getFullYear().toString();
      return firstLetter + lastNameLower + year;
    };
    
    return user;
  }
  const user = createNewUser();
  console.log(`Вік користувача: ${user.getAge()}`);