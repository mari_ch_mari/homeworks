let name = prompt("What is your name?");
while(name.trim()==="" || name === null) {
    name = prompt("Please enter a valid name: ");
}
let age =prompt("What is your age?");

while (age.trim()==="" || age === null || isNaN(age)) {
   
    age = prompt("Please enter a valid age:");
}
age = +age;
if (age < 18){
    alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
    const result = confirm("Are you sure you want to continue?");
    if (result) {
        alert(`Welcome, ${name}!`);
    } else {
        alert("You are not allowed to visit this website.");
    }
} else {
    alert(`Welcome, ${name}!`);
}

