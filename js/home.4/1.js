let num1 = prompt("Enter a number");
let num2 = prompt("Enter another number");
let oper= prompt("Enter the desired operator: '+', '-', '*', '/' or '*', '+'");
oper = String(oper);

while(isNaN(num1)) {
    num1 = prompt("The first value you entered is not a number. Enter again.", num1);
}

while(isNaN(num2)) {
    num1 = prompt("the second value you entered is not number. Enter again.", num2);
}

while (oper != '+' && oper != '-' && oper != '*' && oper != '/') {
    oper = prompt("Unknown operator. Choose among '+', '-', '/', or '*' and enter again." , oper);
} 

num1 = Number(num1);
num2 = Number(num2);

function count(num1 = 1, num2 = 2) {
    let result;
    if (oper == '+') {
        result = num1 + num2;
    } else if (oper == '-') {
        result = num1 - num2;
    } else if (oper == '/') {
        result = num1 / num2; 
    } else if (oper == '*') {
        result = num1 * num2;  
    }

    return console.log(result);
}

count(num1, num2);
  