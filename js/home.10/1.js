const tabs = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tabs-content li');

tabs.forEach((tab) => {
  tab.addEventListener('click', (e) => {
    const selectedTab = e.target;
    const tabId = selectedTab.id;
  
    tabContents.forEach((content) => {
      if (content.getAttribute('data-item') === tabId) {
        content.classList.add('tabs-title-imet');
        content.classList.remove('tabs-title-none');
      } else {
        content.classList.add('tabs-title-none');
        content.classList.remove('tabs-title-imet');
      }
    });
    
    tabs.forEach((tab) => {
      if (tab.id === tabId) {
        tab.classList.add('active');
      } else {
        tab.classList.remove('active');
      }
    });
  });
});









let slider = 1;
function showSlider(item) {
    const ollSlids = document.querySelectorAll('.item-carousel-info');
    const ollSlidsPhoto = document.querySelectorAll('.item');

    if (item > ollSlids.length) {
        slider = 1;
    }
    if (item < 1) {
        slider = ollSlids.length
    }
    console.log(slider);
}
function plus() {
    const ollSlids = document.querySelectorAll('.item-carousel-info');
    const ollSlidsPhoto = document.querySelectorAll('.item');

    ollSlids[slider - 1].classList.toggle('display-none')
    ollSlidsPhoto[slider - 1].classList.toggle('totop')

    showSlider(slider += 1)
    ollSlids[slider - 1].classList.toggle('display-none')
    ollSlidsPhoto[slider - 1].classList.toggle('totop')


}
function minus() {
    const ollSlids = document.querySelectorAll('.item-carousel-info');
    const ollSlidsPhoto = document.querySelectorAll('.item');
    ollSlidsPhoto[slider - 1].classList.toggle('totop')

    ollSlids[slider - 1].classList.toggle('display-none')
    showSlider(slider -= 1)
    ollSlids[slider - 1].classList.toggle('display-none')
    ollSlidsPhoto[slider - 1].classList.toggle('totop')
}
function clickOnSlider(i) {
    const ollSlids = document.querySelectorAll('.item-carousel-info');
    const ollSlidsPhoto = document.querySelectorAll('.item');

    ollSlidsPhoto[slider - 1].classList.toggle('totop')

    ollSlids[slider - 1].classList.toggle('display-none')
    slider = i;
    ollSlids[slider - 1].classList.toggle('display-none')
    ollSlidsPhoto[slider - 1].classList.toggle('totop')
}