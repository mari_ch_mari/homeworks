function createNewUser() {
    const newUser = {
      firstName: '',
      lastName: '',
      getLogin() {
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
      }
    };
  
    newUser.firstName = prompt("Введіть ваше ім'я:");
    newUser.lastName = prompt("Введіть ваше прізвище:");
  
    return newUser;
  }
  
  const user = createNewUser();
  console.log(user.getLogin());
  