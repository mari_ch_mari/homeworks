function printArrayAsList(array, parent = document.body) {
  const list = document.createElement('ul');

  array.map((item) => {
    const listItem = document.createElement('li');
    listItem.innerText = item;
    list.appendChild(listItem);
  });

  parent.appendChild(list);
}
const myArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
printArrayAsList(myArray);