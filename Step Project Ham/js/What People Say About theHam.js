let slider = 1;

function showSlider(item) {
    const allSlides = document.querySelectorAll('.item');
    const allSlidesPhoto = document.querySelectorAll('.nav_slider');
console.log(allSlides.length);
    if (item > allSlides.length) {
        slider = 1;
        console.log(slider);
        console.log(item);
    }
    if (item < 1) {
        slider = allSlides.length;
    }
}

function plus() {
    const allSlides = document.querySelectorAll('.item');
    const allSlidesPhoto = document.querySelectorAll('.nav_slider');

    allSlides[slider - 1].classList.toggle('display-none');
    allSlidesPhoto[slider - 1].classList.toggle('top');

    showSlider(slider += 1);
    allSlides[slider - 1].classList.toggle('display-none');
    allSlidesPhoto[slider - 1].classList.toggle('top');
}

function minus() {
    const allSlides = document.querySelectorAll('.item');
    const allSlidesPhoto = document.querySelectorAll('.nav_slider');

    allSlidesPhoto[slider - 1].classList.toggle('top');
    allSlides[slider - 1].classList.toggle('display-none');

    showSlider(slider -= 1);
    allSlides[slider - 1].classList.toggle('display-none');
    allSlidesPhoto[slider - 1].classList.toggle('top');
}

function clickOnSlider(i) {
    const allSlides = document.querySelectorAll('.item');
    const allSlidesPhoto = document.querySelectorAll('.nav_slider');

    allSlidesPhoto[slider - 1].classList.toggle('top');
    allSlides[slider - 1].classList.toggle('display-none');

    slider = i;
    allSlides[slider - 1].classList.toggle('display-none');
    allSlidesPhoto[slider - 1].classList.toggle('top');
}