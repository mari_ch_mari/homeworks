const workMenu = document.querySelector('.work_menu');

function filter() {
    const menuList = document.querySelectorAll('.our-amazing_work__menu_list');
    workMenu.addEventListener('click', evt => {
        const targetId = evt.target.dataset.id;
        const target = evt.target;
        console.log('FILTER: ', targetId);
        if (target.classList.contains('our-amazing_work__menu_list')) {
            menuList.forEach(menuList => menuList.classList.remove('active'));
            target.classList.add('active');
        }
        switch (targetId) {
            case 'all':
                getWorkItems('amazing_image_item');
                break;
            case 'graphic':
                getWorkItems(targetId);
                break;
            case 'web':
                getWorkItems(targetId);
                break;
            case 'landing':
                getWorkItems(targetId);
                break;
            case 'wordpress':
                getWorkItems(targetId);
                break;
        }
    });
}
console.log('TEST');
filter();

function getWorkItems(className) {
    const workItems = document.querySelectorAll('.amazing_image_item');
    workItems.forEach(item => {
        if (item.classList.contains(className)) {
            item.style.display = 'block';
        } else {
            item.style.display = 'none';
        }
    });
}

function addRow() {
    const workBlocks = document.querySelector('.our-amazing_work__content');
    const workBlocksRow = document.querySelector('.amazing_image_list').cloneNode(true);
    const workGraphic = workBlocksRow.querySelector('.graphic');
    const workWeb = workBlocksRow.querySelector('.web');
    const workLanding = workBlocksRow.querySelector('.landing');
    const workWordpress = workBlocksRow.querySelector('.wordpress');

    workGraphic.querySelector('img').src = './image/graphic-design' + (workBlocks.children.length + 1) + '.jpg';
    workWeb.querySelector('img').src = './image/web-design' + (workBlocks.children.length + 1) + '.jpg';
    workLanding.querySelector('img').src = './image/landing-page' + (workBlocks.children.length + 1) + '.jpg';
    workWordpress.querySelector('img').src = './image/wordpress' + (workBlocks.children.length + 1) + '.jpg';

    workBlocks.appendChild(workBlocksRow);
}

for (let i = 0; i < 2; i++) {
    addRow();
}

// Button-Work Load More
const buttonWork = document.querySelector('.load_btn');

buttonWork.addEventListener('click', evt => {
    evt.preventDefault();
    buttonWork.style.display = 'none';
    for (let i = 0; i < 3; i++) {
        addRow();
    }
});