const searchButton = document.querySelector('.search-button');
const ipDetailsDiv = document.querySelector('.ip-details');

const fetchIPDetails = async () => {
    try {
        const response1 = await fetch('https://api.ipify.org/?format=json');
        const { ip } = await response1.json();
        const response2 = await fetch(`http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district`);
        const data = await response2.json();
        const continent = data.continent || "unknown";
        const country = data.country || "unknown";
        const regionName = data.regionName || "unknown";
        const city = data.city || "unknown";
        const district = data.district || "unknown";

        ipDetailsDiv.innerHTML = `
        <p>${ip}</p>
        <p><strong>Континент:</strong> ${continent}</p>
        <p><strong>Країна:</strong> ${country}</p>
        <p><strong>Регіон:</strong> ${regionName}</p>
        <p><strong>Місто:</strong> ${city}</p>
        <p><strong>Район:</strong> ${district}</p>
        `;
    } catch (error) {
        console.log(error);
        ipDetailsDiv.innerHTML = '<p>Щось пішло не так. Спробуйте знову пізніше.</p>';
    }
};

searchButton.addEventListener('click', fetchIPDetails);

