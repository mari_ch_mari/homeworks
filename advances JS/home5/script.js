const content = document.querySelector('.content');

class Card {
  constructor(card) {
    this.card = card;
    this.li = null;
  }

  createIn(element) {
    this.li = document.createElement('li');
    this.li.classList.add('twit');
    this.li.dataset.userId = this.card.userId;
    this.li.dataset.postId = this.card.postId;

    const button = document.createElement('button');
    button.classList.add('twit__delete');
    button.innerText = 'X';
    button.addEventListener('click', deleteCardHandler);
    this.li.append(button);

    const userPost = document.createElement('div');
    userPost.classList.add('twit__user');
    const userName = document.createElement('span');
    userName.classList.add('user__name');
    userName.innerText = `${this.card.userName}`;
    userPost.append(userName);
    const userEmail = document.createElement('span');
    userEmail.classList.add('user__email');
    userEmail.innerText = `${this.card.userEmail}`;
    userPost.append(userEmail);
    this.li.append(userPost);

    const title = document.createElement('h3');
    title.classList.add('twit__title');
    title.innerText = `${this.card.postTitle}`;
    this.li.append(title);

    const description = document.createElement('div');
    description.classList.add('twit__description');
    description.innerText = this.card.description;
    this.li.append(description);

    element.append(this.li);
  }
}

class CardsListView {
  element;
  constructor(element) {
    this.element = element;
  }

  drawList(cardElements) {
    this.element.innerHTML = '';
    cardElements.forEach((cardElement) => {
      cardElement.createIn(this.element);
    });
  }

  drawAll() {
    const cardElements = [];
    let userAll = [];

    dataLoad.allUsers.then((users) => {
      if (users.length === 0) return;
      userAll = [...users];
    });

    dataLoad.allPosts.then((posts) => {
      if (posts.length === 0) return;
      posts.forEach((post) => {
        let user = userAll.find((item) => item.userId === post.userId);
        Object.assign(post, user);
        cardElements.push(new Card(post));
      });
      this.drawList(cardElements);
    });
  }
}

let dataLoad = {
  urlUsers: 'https://ajax.test-danit.com/api/json/users',
  urlPosts: 'https://ajax.test-danit.com/api/json/posts',

  get allUsers() {
    return fetch(this.urlUsers)
        .then((response) => response.json())
        .then((json) => this.mapArrayUsers(json))
        .catch((error) => console.log(error));
  },

  get allPosts() {
    return fetch(this.urlPosts)
        .then((response) => response.json())
        .then((json) => this.mapArrayPosts(json))
        .catch((error) => console.log(error));
  },

  delete(postId) {
    return fetch(`${this.urlPosts}/${postId}`, {
      method: 'DELETE',
    })
        .then((response) => response.ok)
        .catch((error) => console.log(error));
  },

  mapArrayUsers(array) {
    return array.map((item) => {
      return {
        userId: item.id,
        userName: item.name,
        userEmail: item.email,
      };
    });
  },

  mapArrayPosts(array) {
    return array.map((item) => {
      return {
        postId: item.id,
        userId: item.userId,
        postTitle: item.title,
        description: item.body,
      };
    });
  },
};

function deleteCardHandler(e) {
  let deletePost = e.target.closest(`.${'twit'}`);
  let deleteId = deletePost.dataset.postId;

  dataLoad.delete(deleteId)
      .then((response) => response ? deletePost.remove() : console.log('Error deleting card'))
      .catch((error) => console.log(error));
}

let cardsListView = new CardsListView(content);

window.addEventListener('load', function () {
  cardsListView.drawAll();
});

