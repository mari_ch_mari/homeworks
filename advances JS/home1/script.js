class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    get name() {
      return this._name;
    }
  
    set name(name) {
      this._name = name;
    }
  
    get age() {
      return this._age;
    }
  
    set age(age) {
      this._age = age;
    }
  
    get salary() {
      return this._salary;
    }
  
    set salary(salary) {
      this._salary = salary;
    }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
    }
  
    get salary() {
      return this._salary * 3;
    }
  }

  const programmer1 = new Programmer("Maryna", 31, 100000, ["Python", "JavaScript"]);
  const programmer2 = new Programmer("Alice", 35, 60000, ["Java", "C++"]);
  
  console.log("Programmer 1:");
  console.log("Name:", programmer1.name);
  console.log("Age:", programmer1.age);
  console.log("Salary:", programmer1.salary);
  console.log("Languages:", programmer1._lang);
  
  console.log("Programmer 2:");
  console.log("Name:", programmer2.name);
  console.log("Age:", programmer2.age);
  console.log("Salary:", programmer2.salary);
  console.log("Languages:", programmer2._lang);
  